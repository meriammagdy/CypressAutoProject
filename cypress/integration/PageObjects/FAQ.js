class FAQ{

    ClickonButton(locator){    //function for clicking help menu 

        cy.wait(700)
        cy.get(locator).click()
       
    }
     


     AssertQuestionandAnswer(Questionlocator,Answerlocator){   
        cy.wait(700)
        cy.get(Questionlocator).click()          //click on  question
        cy.get(Answerlocator).should('be.visible')  //check if the  paragraph is visible 
     }
      
   
       
    

     ScrollandAssert(Questionlocator,Answerlocator){

    cy.wait(700)
    cy.get(Questionlocator).scrollIntoView({ duration: 5000 }).click()     ////click on fourth question 
    cy.get(Answerlocator).should('be.visible')            //check if the fourth paragraph is visible 

     }

    

     

    
    }

    


export default FAQ