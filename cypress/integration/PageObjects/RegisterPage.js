


class RegisterPage{

 
    ClickonRegisterbutton(registerbuttonlocator){

        cy.get(registerbuttonlocator).click()
    }

    ChooseStudentRegForm(studentbuttonlocator){

        cy.wait(1000)

        cy.get(studentbuttonlocator).click()
    }

    FillField(fieldlocator,data){

        cy.get(fieldlocator).type(data)
    

        return this

    }
   

   ScrollandFill(locator,data){

        
        cy.get(locator).scrollIntoView({ duration: 5000 }).type(data)
        cy.wait(1000)
        return this
    }

    SelectFemaleGender(femalebuttonlocator){
        cy.wait(1000)
        cy.get(femalebuttonlocator).scrollIntoView({ duration: 5000 }).click()
        
    }

    SelectGradefromdropdown(gradedropdownlistlocator,grade){

       cy.get(gradedropdownlistlocator).select(grade)
    }

   // FillPasswordField(passwordlocator,password){
        

       // cy.get(passwordlocator).scrollIntoView({ duration: 5000 }).type(password)
   // }

   // FillConfirmPasswordField(confirmpasswordlocator,confirmpassword){

        //cy.get(confirmpasswordlocator).scrollIntoView({ duration: 5000 }).type(confirmpassword)
   // }


    SolveRecaptcha(){
        cy.get('#recaptcha-anchor > div.recaptcha-checkbox-border').click()
    }

}

export default RegisterPage