
import RegisterPage from '../PageObjects/RegisterPage'
import Fixture from '../PageObjects/Fixture'


 beforeEach(() => {                                    //run before every

const fix = new Fixture()    //object from fixture class
fix.Getdataandselectors()    //call function of get data and selectors

  
    
  })
describe('Register page',function(){          //creating Register Suite
 
   
    it('first Scenario :User can register as a student when filling the valid data ',function(){                           

     const lp = new RegisterPage()                                             //obj for registerpage class
        lp.ClickonRegisterbutton(this.selector.Registerbutton)                 //passing the selector of registerbutton to function to click on the button
        lp.ChooseStudentRegForm(this.selector.Studentbutton)                   //passing the selector of studentbutton to function to click on student button   
        lp.FillField(this.selector.firstnamelocator,this.data.firstname)    //passing the selector and data of firstnamefield to function to type the data in the field
        lp.FillField(this.selector.lastnamelocator,this.data.lastname)        //passing the selector and data of lastnamefield to function to type the data in the field

        lp.ScrollandFill(this.selector.phonenumberlocator,this.data.phonenumber)      //passing the selector and data of phonenumberfield to function to type the data in the field
        lp.SelectFemaleGender(this.selector.Femalegenderbuttonlocator)                  //passing the selector of femalegenderbutton to function to click on the button
        lp. SelectGradefromdropdown(this.selector.gradedropdownlistlocator,this.data.grade)              //passing the selector of grade dropdown list
        lp.ScrollandFill(this.selector.passwordlocator,this.data.password)         //passing the selector and data of password field to function to type the data in the field
        lp.ScrollandFill(this.selector.confirmpasswordlocator,this.data.confirmpassword)  //passing the selector and data of confirm password field to function to type the data in the field

    })})
  