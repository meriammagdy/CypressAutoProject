

import FAQ from '../PageObjects/FAQ'
import Fixture from '../PageObjects/Fixture'

beforeEach(() => {                                    //run before every

    const fix = new Fixture()    //object from fixture class
fix.Getdataandselectors()    //call function of get data and selectors
 
 })   

describe('FAQTestCases',function(){

    
            it('Verify that when you click on first question the first answer appears',function(){
                
               const FaqObj= new FAQ() 
    
                FaqObj.ClickonButton(this.selector.helpmenulocator)   //passing the selector of help menu to function to click on it
                FaqObj.ClickonButton(this.selector.FAQlocator)             //passing the selectot of FAQlocator to function to click on it 
                FaqObj. AssertQuestionandAnswer(this.selector.firstquestionlocator,this.selector.Answerfirstquestionlocator)    //passing the selector of firstquestion and first answer 
                FaqObj.AssertQuestionandAnswer(this.selector.secondquestionlocator,this.selector.Answerseondquestionlocator)  //passing the selector of secondquestion and second answer    
                FaqObj.ScrollandAssert(this.selector.thirdQuestionlocator,this.selector.Answerthirdquestionlocator)   //passing the selector of thirdquestion and third answer
                FaqObj.ScrollandAssert(this.selector.fourthQuestionlocator,this.selector.Answerfourthquestionlocator)   //passing the selector of fourthquestion and fourth answer
                FaqObj.ScrollandAssert(this.selector.fifthQuestionlocator,this.selector.Answerfifthquestionlocator)    ////passing the selector of fifthquestion and fifth answer
                FaqObj.ScrollandAssert(this.selector.sixthQuestionlocator,this.selector.Answersixthquestionlocator)  //passing the selector of sixthquestion and sixth answer
               FaqObj.ScrollandAssert(this.selector.seventhQuestionlocator,this.selector.Answerseventhquestionlocator)   //passing the selector of seventh question and seventh answer
    
    
    
                    })
            
        
    
                })
                
        
            
    